import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by me on 2015-09-28.
 */
public class JdbcContext {

    SimpleDriverDataSource dataSource;

    public void setDataSource(SimpleDriverDataSource dataSource) {
        this.dataSource = dataSource;
    }


    public void executeSql(final String sql) throws SQLException {
        StatementStrategy stmt = new StatementStrategy() {
            @Override
            public PreparedStatement makePreparedStatement(Connection c) throws SQLException {
                return c.prepareStatement(sql);
            }
        };

        workWithStatementStrategy(stmt);
    }

    public void workWithStatementStrategy(StatementStrategy stmt) throws SQLException {
        Connection c = dataSource.getConnection();
        PreparedStatement ps = null;

        try {
            ps = stmt.makePreparedStatement(c);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if(ps != null){
                try {
                    ps.close();
                } catch (SQLException e) {
                }
            }

            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                }
            }
        }
    }
}
