import java.util.List;

/**
 * Created by me on 2015-09-29.
 */
public interface UserDao {
    void add(User user);
    User get(String id);
    List<User> getAll();
    void deleteAll();
    int getCount();
}
