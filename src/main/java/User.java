/**
 * Created by me on 2015-04-18.
 */
public class User {
    private String id;
    private String password;
    private String name;
    Level level;
    int login;
    int recommend;

    public User(String id, String password, String name, Level level, int login, int recommend) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.level = level;
        this.login = login;
        this.recommend = recommend;
    }

    public User(){

    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
