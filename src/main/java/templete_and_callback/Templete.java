package templete_and_callback;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by me on 2015-09-28.
 */
public class Templete {
    public <T>T lineReadTemplete(String filePath, LineCallback<T> callback, T initVal) throws IOException {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(filePath));
            T res = initVal;
            String line = null;
            while( (line=br.readLine()) != null){
                res = callback.doSomethingWithLine(line, res);
            }
            return res;

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            throw e;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        } finally {
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

}
