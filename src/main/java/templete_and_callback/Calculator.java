package templete_and_callback;

import javax.sound.sampled.Line;
import java.io.*;

/**
 * Created by me on 2015-09-28.
 */
public class Calculator {

    private Templete templete;

    public Calculator() {
        this.templete = new Templete();
    }

    public String concatenate(String path) throws IOException {
        LineCallback<String> lineCallback = new LineCallback<String>() {
            @Override
            public String doSomethingWithLine(String line, String value) {
                value += line;
                return value;
            }
        };

        return this.templete.lineReadTemplete(path, lineCallback, "");
    }


    public int calcMultiple(String path) throws IOException {
        LineCallback<Integer> lineCallback = new LineCallback<Integer>() {
            @Override
            public Integer doSomethingWithLine(String line, Integer value) {
                value *= Integer.valueOf(line);
                return value;
            }
        };

        return this.templete.lineReadTemplete(path, lineCallback, 1);
    }

    public Integer calcSum(String path) throws IOException {
        LineCallback<Integer> lineCallback = new LineCallback<Integer>() {
            @Override
            public Integer doSomethingWithLine(String line, Integer value) {
                value += Integer.valueOf(line);
                return value;
            }
        };

        return this.templete.lineReadTemplete(path, lineCallback, 0);
    }

}