package templete_and_callback;

/**
 * Created by me on 2015-09-28.
 */
public interface LineCallback<T> {
     T doSomethingWithLine(String line, T value);
}
