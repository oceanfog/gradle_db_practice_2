import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * Created by me on 2015-04-18.
 */
public class UserDaoJdbcTest {
    ApplicationContext context;
    UserDaoJdbc dao;
    User user1, user2, user3;

    @Before
    public void setup(){
        this.context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        this.dao = context.getBean("userDao", UserDaoJdbc.class);
        this.user1=new User("oceanfog", "krkim", "1123", Level.BASIC, 1, 0);
        this.user2=new User("oceanfog2", "krkim", "1123", Level.BASIC, 55, 10);
        this.user3=new User("oceanfog3", "krkim", "1123", Level.BASIC, 100, 40);

    }

    @Test
    public void test() throws SQLException, ClassNotFoundException {
        dao.deleteAll();

        assertEquals(0, dao.getCount());


        dao.add(user1);
        assertEquals(1, dao.getCount());

        User user = dao.get("oceanfog");
        assertEquals(user1.getId(), user.getId());
    }
}
