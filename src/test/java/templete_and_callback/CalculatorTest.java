package templete_and_callback;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class CalculatorTest {

    //define Calculator class
    //file 경로를 받아서 써있는 숫자들의 합을 구하는 Program

    @Test
    public void sumOfNumbers() throws IOException {
        Calculator calculator = new Calculator();

        File file = new File("./numbers.txt");

        int sum = calculator.calcSum(file.getPath());
        int multiple = calculator.calcMultiple(file.getPath());
        String concatenate = calculator.concatenate(file.getPath());

        assertEquals(10, sum);
        assertEquals(24, multiple);
        assertEquals("1234", concatenate);

    }


}